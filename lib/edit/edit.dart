import 'package:flutter/material.dart';
import 'package:sqflite_test/model/DogModel.dart';
import 'package:sqflite_test/service/databaseService.dart';

class EditContent extends StatefulWidget {
  Dog dog;
  EditContent(this.dog);

  @override
  State<EditContent> createState() => _EditContentState();
}

class _EditContentState extends State<EditContent> {
  DatabaseService db = DatabaseService();
  String name = '';
  int age = 0;
  String breed = '';

  _editContent() async{
    Dog dog = Dog(id: widget.dog.id, name: name, age: age, breed: breed);
    db.updateDog(dog);
    Navigator.pop(context);
  }

  _deleteContent(int id) async{
    db.deleteDog(id);
    Navigator.pop(context);
  }

  @override
  void initState(){
    setState((){
      name= widget.dog.name;
      age= widget.dog.age;
      breed= widget.dog.breed;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Dog editDog = widget.dog;
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Form(
            child: Column(
              children: [
                const Text(
                  'Edit content',
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.w500),
                ),
                TextFormField(
                  initialValue: editDog.name,
                  onChanged: (value) {
                    setState(() {
                      name = value;
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Name',
                    labelText: 'Name',
                  ),
                ),
                TextFormField(
                  initialValue: '${editDog.age}',
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    setState(() {
                      age = int.parse(value);
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Age',
                    labelText: 'Age',
                  ),
                ),
                TextFormField(
                  initialValue: editDog.breed,
                  onChanged: (value) {
                    setState(() {
                      breed = value;
                    });
                  },
                  decoration: const InputDecoration(
                    hintText: 'Breed',
                    labelText: 'Breed',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(onPressed: (){
                      _deleteContent(editDog.id);
                    }, style: ElevatedButton.styleFrom(
                      primary: Colors.redAccent
                    ),child: const Text('Delete')),
                    ElevatedButton(onPressed: _editContent, child: const Text('Edit'))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
