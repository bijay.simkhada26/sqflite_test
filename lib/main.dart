import 'package:flutter/material.dart';
import 'package:sqflite_test/edit/edit.dart';
import 'package:sqflite_test/model/DogModel.dart';
import 'package:sqflite_test/service/databaseService.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  DatabaseService db = DatabaseService();
  db.dbopen();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const DogTest(),
    );
  }
}

class DogTest extends StatefulWidget {
  const DogTest({Key? key}) : super(key: key);

  @override
  State<DogTest> createState() => _DogTestState();
}

class _DogTestState extends State<DogTest> {
  DatabaseService db = DatabaseService();

  String name = '';
  int age = 0;
  String breed = '';

  List<Dog> _dogList = [];

  _testDB() async {
    await db.insertDog(
        Dog(id: UniqueKey().hashCode, name: name, age: age, breed: breed));
  }

  _retrieveDogs() async {
    var result = await db.retrieveDogs();
    setState(() {
      _dogList = result;
    });
    print(_dogList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          // height: MediaQuery.of(context).size.height-30,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              const Text(
                'SQFLITE Test',
                style: TextStyle(fontSize: 24),
              ),
              Form(
                  child: Container(
                child: Column(
                  children: [
                    TextFormField(
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      decoration: const InputDecoration(
                          hintText: 'Name', labelText: 'Name'),
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          age = int.parse(value);
                        });
                      },
                      decoration: const InputDecoration(
                          hintText: 'Age', labelText: 'Age'),
                    ),
                    TextFormField(
                      onChanged: (value) {
                        setState(() {
                          breed = value;
                        });
                      },
                      decoration: const InputDecoration(
                          hintText: 'Breed', labelText: 'Breed'),
                    ),
                  ],
                ),
              )),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: TextButton(
                  onPressed: _testDB,
                  style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blueAccent),
                  child: const Text(
                    'Post',
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: _retrieveDogs,
                child: const Text(
                  'Refresh',
                  style: TextStyle(fontSize: 30),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              _dogList.isNotEmpty
                  ? SingleChildScrollView(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: _dogList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            child: ListTile(
                              leading: const Icon(Icons.pets_rounded),
                              title: Text(_dogList[index].name),
                              subtitle: Text(
                                  '${_dogList[index].breed} Age: ${_dogList[index].age}'),
                              trailing: IconButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              EditContent(_dogList[index])));
                                },
                                icon: const Icon(Icons.edit),
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : const Text(
                      'Retrieve dogs to see content',
                      style: TextStyle(fontSize: 20),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
