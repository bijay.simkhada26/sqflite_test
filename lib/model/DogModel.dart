class Dog{
  final int id;
  final String name;
  final int age;
  final String breed;

  Dog({required this.id, required this.name, required this.age, required this.breed });

  Map<String, dynamic> toMap(){
    return {
      'id': id,
      'name': name,
      'age': age,
      'breed': breed
    };
  }

  @override
  String toString(){
    return 'Dog(id: $id, name:$name, age:$age, breed:$breed)';
  }
}