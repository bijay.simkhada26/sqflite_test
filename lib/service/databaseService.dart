import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_test/model/DogModel.dart';

class DatabaseService {
  var database;

  Future<Database> dbopen() async {
    database =
        await openDatabase(join(await getDatabasesPath(), 'dog_database.db'),
            onCreate: (db, version) {
      return db.execute('CREATE TABLE dogs('
          'id INTEGER PRIMARY KEY, name TEXT, age INTEGER, breed TEXT'
          ')');
    }, version: 1);
    if (database != null) {
      print('sucessfully connected to the db');
      return database;
    } else {
      print('unable to connect to the db');
      return database;
    }
  }

  Future<void> insertDog(Dog dog) async {
    final Database db = await dbopen();
    var result = await db.insert(
      'dogs',
      dog.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print(result);
    // final result = db.rawInsert(
    //     'INSERT INTO dogs( id, name, age, breed ) VALUES (${dog.id}, ${dog.name}, ${dog.age}, ${dog.breed})');
  }

  Future<List<Dog>> retrieveDogs() async {
    final Database db = await dbopen();
    final List<Map<String, dynamic>> queryResult = await db.query('dogs');
    return List.generate(queryResult.length, (index) {
      return Dog(
          id: queryResult[index]['id'],
          name: queryResult[index]['name'],
          age: queryResult[index]['age'],
          breed: queryResult[index]['breed']);
    });
  }

  Future<void> updateDog(Dog dog) async{
    final Database db = await dbopen();
    await db.update('dogs', dog.toMap(), where: 'id = ?', whereArgs: [dog.id]);
  }

  Future<void> deleteDog(int id) async{
    final Database db = await dbopen();
    await db.delete('dogs', where: 'id = ? ', whereArgs: [id]);
  }
}
